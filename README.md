# NAME

wxC

# VERSION

0.93.0.0-8

# DESCRIPTION OF ORIGINAL PROJECT

The wxWidgets project provides a popular C++ based framework for GUI
programming. In order to port this framework to the Eiffel, Haskell, Ocaml
programming languages some efforts have been made in the ELJ and wxHaskell
projects. Because it was not possible to use C++ directly from these languages
an interfacing layer was written in C in order to produce libraries for linking
the functionality of wxWidgets dynamically resp. statically. Originally these
projects used the same code base but have diverged in implementation details.
The wxC project is the attempt to join the efforts again.

The long-term goal is to provide C based import libraries for wxWidgets which
can be used in any environment that is unable to bind to the original C++
framework.

# DESCRIPTION OF THIS PROJECT

The page your are viewing is dedicated to fork of the wxC which is part of
wxHaskell. And it is continuation of idea of the original wxC.

This fork was created due to several reasons.

+ Original wxC has not been updated for 20 years.
  wxC which is part of wxHaskell has not been updated for 3 years. Is it dead
  too?

+ Nor original wxC nor wxHaskel are buildable at least by this moment.

+ Move wxC codebase outside the wxHaskell project. Not everybody wants to
  download Cabal and Haskell toolkit to build the code, because, in fact, only
  C++ compiler is needed for that. For example, I needed the wxC for building
  my own instant messenger named yat. It's written in Vala, not Haskell. So why
  should I download GHC and go through wxHaskell dependencies?

+ Add support of some wxWidgets functional, which is absent in original wxC. For
  example, wxFilePickerCtrl, wxAboutDialog, wxSearchCtrl and markup methods.

+ Add packaging. Debian, RPM-based, Slackware and other GNU systems, M$ Windows,
  Apple macOS, maybe some UNIX: all these OS are needed in distribution of
  binaries forms of wxC. Original wxC doesn't build any binary packages. We do.
  Note that builds are necessary for different hardware architectures, not just
  amd64, I mean arm64/aarch64 at least.

# INSTALL

INSTALL file describes how to install the wxC from scratch.

You do not need it to use wxC.

Download binaries and skip all the INSTALL instructions:

+ **Deb-based GNU/Linux distro**

  ![Alt Text](doc/logo-debian.png "Logo Debian")

  http://www.lovecry.pt/dl/libwxc_0.93.0.0-8-1_amd64.deb |
  http://www.lovecry.pt/dl/libwxc-dev_0.93.0.0-8-1_amd64.deb

  http://www.lovecry.pt/dl/libwxc_0.93.0.0-8-1_arm64.deb |
  http://www.lovecry.pt/dl/libwxc-dev_0.93.0.0-8-1_arm64.deb

+ **RPM-based GNU/Linux distro**
  *(Not tested!)*

  ![Alt Text](doc/logo-redhat.png "Logo Red Hat")

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8-2.x86_64.rpm |
  http://www.lovecry.pt/dl/libwxc-dev-0.93.0.0.8-2.x86_64.rpm

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8-2.arm64.rpm |
  http://www.lovecry.pt/dl/libwxc-dev-0.93.0.0.8-2.arm64.rpm

+ **Slackware and source-based GNU/Linux distros**
  *(Not tested!)*

  ![Alt Text](doc/logo-slackware.png "Logo Slackware")

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8.amd64.tgz |
  http://www.lovecry.pt/dl/libwxc-dev-0.93.0.0.8.amd64.tgz

  http://www.lovecry.pt/dl/libwxc-0.93.0.0.8.arm64.tgz |
  http://www.lovecry.pt/dl/libwxc-dev-0.93.0.0.8.arm64.tgz

+ **M$ Windows**

  ![Alt Text](doc/logo-win.png "Logo Windows") http://www.lovecry.pt/dl/libwxc_0.93.0.0-7_amd64.zip

+ **Apple macOS**

  TODO

# COPYRIGHT AND LICENSE

wxWidgets Licence
