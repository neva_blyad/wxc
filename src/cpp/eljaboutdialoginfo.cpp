#include "wrapper.h"

extern "C"
{

EWXWEXPORT(wxAboutDialogInfo*,wxAboutDialogInfo_Create)()
{
	return new wxAboutDialogInfo ();
}

EWXWEXPORT(void,wxAboutDialogInfo_Delete)(wxAboutDialogInfo* self)
{
	delete self;
}

EWXWEXPORT(void,wxAboutDialogInfo_SetIcon)(wxAboutDialogInfo* self, wxIcon* icon)
{
	self->SetIcon(*icon);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetName)(wxAboutDialogInfo* self, wxString* name)
{
	self->SetName(*name);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetVersion)(wxAboutDialogInfo* self, wxString* ver)
{
	self->SetVersion(*ver);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetDescription)(wxAboutDialogInfo* self, wxString* desc)
{
	self->SetDescription(*desc);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetWebSite)(wxAboutDialogInfo* self, wxString* url)
{
	self->SetWebSite(*url);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetCopyright)(wxAboutDialogInfo* self, wxString* copyright)
{
	self->SetCopyright(*copyright);
}

EWXWEXPORT(void,wxAboutDialogInfo_AddDeveloper)(wxAboutDialogInfo* self, wxString* developer)
{
	self->AddDeveloper(*developer);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetArtists)(wxAboutDialogInfo* self, wxArrayString* artists)
{
	self->SetArtists(*artists);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetTranslators)(wxAboutDialogInfo* self, wxArrayString* translators)
{
	self->SetTranslators(*translators);
}

EWXWEXPORT(void,wxAboutDialogInfo_SetLicense)(wxAboutDialogInfo* self, wxString* license)
{
	self->SetLicense(*license);
}

EWXWEXPORT(void,wxAboutBox)(wxAboutDialogInfo* info, wxWindow* parent)
{
	wxAboutBox(*info, parent);
}

}

