#include "wrapper.h"

extern "C"
{

EWXWEXPORT(void*,wxSearchCtrl_Create)(wxWindow* _prt,int _id,wxString* _txt,int _lft,int _top,int _wdt,int _hgt,long _stl)
{
	return (void*) new wxSearchCtrl (_prt, _id, *_txt, wxPoint(_lft, _top), wxSize(_wdt, _hgt), _stl, wxDefaultValidator);
}

EWXWEXPORT(int,wxSearchCtrl_EmulateKeyPress)(wxSearchCtrl* self,wxKeyEvent* keyevent)
{
    return self->EmulateKeyPress(*keyevent);
}

EWXWEXPORT(void*,wxSearchCtrl_GetDefaultStyle)(wxSearchCtrl* self)
{
    return (void*)& self->GetDefaultStyle();
}

EWXWEXPORT(wxString*,wxSearchCtrl_GetRange)(wxSearchCtrl* self,long from,long to)
{
    wxString *result = new wxString();
    *result = self->GetRange(from, to);
    return result;
}

EWXWEXPORT(wxString*,wxSearchCtrl_GetStringSelection)(wxSearchCtrl* self)
{
    wxString *result = new wxString();
    *result = self->GetStringSelection();
    return result;
}

EWXWEXPORT(bool,wxSearchCtrl_IsMultiLine)(wxSearchCtrl* self)
{
    return  self->IsMultiLine();
}

EWXWEXPORT(bool,wxSearchCtrl_IsSingleLine)(wxSearchCtrl* self)
{
    return self->IsSingleLine(   );
}

EWXWEXPORT(bool,wxSearchCtrl_SetDefaultStyle)(wxSearchCtrl* self,wxTextAttr* style)
{
    return self->SetDefaultStyle(*style);
}

EWXWEXPORT(void,wxSearchCtrl_SetMaxLength)(wxSearchCtrl* self,long len)
{
    self->SetMaxLength( len  );
}

EWXWEXPORT(bool,wxSearchCtrl_SetStyle)(wxSearchCtrl* self,long start,long end,wxTextAttr* style)
{
    return self->SetStyle(start, end,*style);
}

EWXWEXPORT(wxString*,wxSearchCtrl_GetValue)(void* self)
{
	wxString *result = new wxString();
	*result = ((wxSearchCtrl*)self)->GetValue();
	return result;
}
	
EWXWEXPORT(void,wxSearchCtrl_SetValue)(wxSearchCtrl* self,wxString* value)
{
	self->SetValue(*value);
}

EWXWEXPORT(void,wxSearchCtrl_ChangeValue)(wxSearchCtrl* self, wxString* value)
{
	self->ChangeValue(*value);
}
	
EWXWEXPORT(int,wxSearchCtrl_GetLineLength)(wxSearchCtrl* self,long lineNo)
{
	return self->GetLineLength(lineNo);
}
	
EWXWEXPORT(wxString*,wxSearchCtrl_GetLineText)(wxSearchCtrl* self,long lineNo)
{
	wxString *result = new wxString();
	*result = self->GetLineText(lineNo);
	return result;
}
	
EWXWEXPORT(int,wxSearchCtrl_GetNumberOfLines)(wxSearchCtrl* self)
{
	return self->GetNumberOfLines();
}
	
EWXWEXPORT(bool,wxSearchCtrl_IsModified)(wxSearchCtrl* self)
{
	return self->IsModified();
}
	
EWXWEXPORT(bool,wxSearchCtrl_IsEditable)(wxSearchCtrl* self)
{
	return self->IsEditable();
}
	
EWXWEXPORT(void,wxSearchCtrl_GetSelection)(wxSearchCtrl* self,void* from,void* to)
{
	self->GetSelection((long*)from, (long*)to);
}
	
EWXWEXPORT(void,wxSearchCtrl_Clear)(wxSearchCtrl* self)
{
	self->Clear();
}
	
EWXWEXPORT(void,wxSearchCtrl_Replace)(wxSearchCtrl* self,long from,long to,wxString* value)
{
	self->Replace(from, to,*value);
}
	
EWXWEXPORT(void,wxSearchCtrl_Remove)(wxSearchCtrl* self,long from,long to)
{
	self->Remove(from, to);
}
	
EWXWEXPORT(bool,wxSearchCtrl_LoadFile)(wxSearchCtrl* self,wxString* file)
{
	return self->LoadFile(*file);
}
	
EWXWEXPORT(bool,wxSearchCtrl_SaveFile)(wxSearchCtrl* self,wxString* file)
{
	return self->SaveFile(*file);
}
	
EWXWEXPORT(void,wxSearchCtrl_DiscardEdits)(wxSearchCtrl* self)
{
	self->DiscardEdits();
}
	
EWXWEXPORT(void,wxSearchCtrl_WriteText)(wxSearchCtrl* self,wxString* text)
{
	self->WriteText(*text);
}
	
EWXWEXPORT(void,wxSearchCtrl_AppendText)(wxSearchCtrl* self,wxString* text)
{
	self->AppendText(*text);
}
	
EWXWEXPORT(long,wxSearchCtrl_XYToPosition)(wxSearchCtrl* self,long x,long y)
{
	return self->XYToPosition(x, y);
}
	
EWXWEXPORT(int,wxSearchCtrl_PositionToXY)(wxSearchCtrl* self,long pos,long* x,long* y)
{
	return (int)self->PositionToXY(pos, x, y);
}
	
EWXWEXPORT(void,wxSearchCtrl_ShowPosition)(wxSearchCtrl* self,long pos)
{
	self->ShowPosition(pos);
}
	
EWXWEXPORT(void,wxSearchCtrl_Copy)(wxSearchCtrl* self)
{
	self->Copy();
}
	
EWXWEXPORT(void,wxSearchCtrl_Cut)(wxSearchCtrl* self)
{
	self->Cut();
}
	
EWXWEXPORT(void,wxSearchCtrl_Paste)(wxSearchCtrl* self)
{
	self->Paste();
}
	
EWXWEXPORT(bool,wxSearchCtrl_CanCopy)(wxSearchCtrl* self)
{
	return self->CanCopy();
}
	
EWXWEXPORT(bool,wxSearchCtrl_CanCut)(wxSearchCtrl* self)
{
	return self->CanCut();
}
	
EWXWEXPORT(bool,wxSearchCtrl_CanPaste)(wxSearchCtrl* self)
{
	return self->CanPaste();
}
	
EWXWEXPORT(void,wxSearchCtrl_Undo)(wxSearchCtrl* self)
{
	self->Undo();
}
	
EWXWEXPORT(void,wxSearchCtrl_Redo)(wxSearchCtrl* self)
{
	self->Redo();
}
	
EWXWEXPORT(bool,wxSearchCtrl_CanUndo)(wxSearchCtrl* self)
{
	return self->CanUndo();
}
	
EWXWEXPORT(bool,wxSearchCtrl_CanRedo)(wxSearchCtrl* self)
{
	return self->CanRedo();
}
	
EWXWEXPORT(void,wxSearchCtrl_SetInsertionPoint)(wxSearchCtrl* self,long pos)
{
	self->SetInsertionPoint(pos);
}
	
EWXWEXPORT(void,wxSearchCtrl_SetInsertionPointEnd)(wxSearchCtrl* self)
{
	self->SetInsertionPointEnd();
}
	
EWXWEXPORT(long,wxSearchCtrl_GetInsertionPoint)(wxSearchCtrl* self)
{
	return self->GetInsertionPoint();
}
	
EWXWEXPORT(long,wxSearchCtrl_GetLastPosition)(wxSearchCtrl* self)
{
	return self->GetLastPosition();
}
	
EWXWEXPORT(void,wxSearchCtrl_SetSelection)(wxSearchCtrl* self,long from,long to)
{
	self->SetSelection(from, to);
}
	
EWXWEXPORT(void,wxSearchCtrl_SetEditable)(wxSearchCtrl* self,bool editable)
{
	self->SetEditable(editable);
}
	
}
