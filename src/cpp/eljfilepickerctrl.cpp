#include "wrapper.h"

extern "C"
{

EWXWEXPORT(wxString*,wxFilePickerCtrl_GetPath)(void* _obj)
{
	wxString *result = new wxString();
	*result = ((wxFilePickerCtrl*)_obj)->GetPath();
	return result;
}

EWXWEXPORT(void,wxFilePickerCtrl_SetPath)(void* _obj, wxString* filename)
{
	((wxFilePickerCtrl*)_obj)->SetPath(*filename);
}

}
