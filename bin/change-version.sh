#!/usr/bin/env bash
#
# Upgrades the program version in all files in repository.

function use
{
    echo "Usage: $0 <FORK>"
    exit 1
}

if [ $# -ne 1 ]
then
    use
fi

MAJOR=0
MINOR=93
MICRO=0
NANO=0
FORK=8

UPSTREAM="$MAJOR.$MINOR.$MICRO.$NANO"

perl -e 'my $name;
         local $/;
         $_ = <STDIN>;
         s|# VERSION\n\n\K\d+\.\d+\.\d+\.\d+-\d+( "[^"]+")?|$ARGV[0]-$ARGV[1]|;
         print' "$UPSTREAM" "$FORK" < README.md > README.md.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|major = \K\d+|$ARGV[0]|;
         s|minor = \K\d+|$ARGV[1]|;
         s|micro = \K\d+|$ARGV[2]|;
         s|nano  = \K\d+|$ARGV[3]|;
         s|fork  = \K\d+|$ARGV[4]|;
         print' "$MAJOR" "$MINOR" "$MICRO" "$NANO" "$FORK" < SConstruct > SConstruct.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+\.\d+-\d+-\d+|$ARGV[0].$ARGV[1]-1|;
         print' "$UPSTREAM" "$FORK" < debian/DEBIAN/control > debian/DEBIAN/control.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+\.\d+-\d+-\d+|$ARGV[0].$ARGV[1]-1|;
         s|Depends: libwxc \(= \K\d+\.\d+\.\d+\.\d+-\d+-\d+\)|$ARGV[0].$ARGV[1]-1)|;
         print' "$UPSTREAM" "$FORK" < debian-dev/DEBIAN/control > debian-dev/DEBIAN/control.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+\.\d+-\d+|$ARGV[0]-$ARGV[1]|;
         print' "$UPSTREAM" "$FORK" < wxc.pc.unix > wxc.pc.unix.new

perl -e 'local $/;
         $_ = <STDIN>;
         s|Version: \K\d+\.\d+\.\d+\.\d+-\d+|$ARGV[0]-$ARGV[1]|;
         print' "$UPSTREAM" "$FORK" < wxc.pc.win > wxc.pc.win.new

mv debian/DEBIAN/control.new     debian/DEBIAN/control
mv debian-dev/DEBIAN/control.new debian-dev/DEBIAN/control
mv README.md.new                 README.md
mv SConstruct.new                SConstruct
mv wxc.pc.unix.new               wxc.pc.unix
mv wxc.pc.win.new                wxc.pc.win

echo Done
