import SCons
import os
import sys

# Construct the environment
env = Environment(ENV={'PATH'            : os.environ['PATH'],
                       'PKG_CONFIG_PATH' : os.environ['PKG_CONFIG_PATH']})

# Software version
major = 0
minor = 93
micro = 0
nano  = 0
fork  = 8

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons lib-static                                                             #
################################################################################

# Determine wxWidgets dependencies
env.ParseConfig('wx-config --version=3.0 --libs all --cppflags')

# External macro definitions
env.Append(CPPDEFINES = 'wxUSE_FILEPICKERCTRL') # wxFilePickerCtrl
env.Append(CPPDEFINES = 'wxUSE_SEARCHCTRL')     # wxSearchCtrl

# Add wxC includes and sources
env.Append(CPPPATH = 'src/include/')
src = Glob('src/cpp/*.cpp')

# Add static library target
target_lib_static = env.Library('wxc', src)
env.Alias('lib-static', target_lib_static)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons lib-shared                                                             #
################################################################################

# Add shared library target
ver = str(major) + '.' + str(minor) + '.' + str(micro) + '.' + str(nano) + '-' + str(fork)
target_lib_shared = env.SharedLibrary('wxc', src, SHLIBVERSION=ver)
env.Alias('lib-shared', target_lib_shared)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons                                                                        #
# scons all                                                                    #
################################################################################

# Add all target
target_all = [target_lib_static, target_lib_shared]
env.Alias('all', target_all)
env.Default(target_all) # Invoking scons without arguments will produce static and shared library

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons install                                                                #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Configure installation logic
    AddOption('--prefix', dest    = 'prefix',
                          nargs   = 1,
                          type    = 'string',
                          action  = 'store',
                          metavar = 'DIR',
                          help    = 'Installation prefix',
                          default = '/usr/local/')
    AddOption('--arch',   dest    = 'arch',
                          nargs   = 1,
                          type    = 'string',
                          action  = 'store',
                          metavar = 'DIR',
                          help    = 'Hardware architecture',
                          default = 'x86_64-linux-gnu')

    prefix = str(GetOption('prefix'))
    arch   = str(GetOption('arch'))

    # Prepare installation paths
    dir_lib              = os.path.join(prefix,        'lib/')
    dir_lib_arch         = os.path.join(dir_lib,       arch)
    dir_share            = os.path.join(prefix,        'share/')
    dir_share_doc        = os.path.join(dir_share,     'doc/')
    dir_share_doc_libwxc = os.path.join(dir_share_doc, 'libwxc/')

    # Make symbolic link
    def sym_link(target, source, env):
        os.symlink(str(source[0]), str(target[0]))

    # Add install target
    target_install_all = [env.Install(dir_lib_arch, target_lib_shared),
                          env.Command(target = os.path.join(dir_lib_arch, 'libwxc.so.' + str(major)),
                                      source = target_lib_shared,
                                      action = [sym_link]),
                          env.Install(dir_share_doc_libwxc, ['LICENSE',
                                                             'README.md'])]
    dst = env.Alias('install', target_install_all)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons install-dev                                                            #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Prepare installation paths
    dir_inc                  = os.path.join(prefix,        'include/')
    dir_inc_wxc              = os.path.join(dir_inc,       'wxc/')
    dir_share_doc_libwxc_dev = os.path.join(dir_share_doc, 'libwxc-dev/')
    dir_lib_arch_pkg         = os.path.join(dir_lib_arch,  'pkgconfig/')

    # Add install target (development files)
    target_install_dev = [env.Install(dir_inc_wxc, Glob('src/include/*.h')),
                          env.Install(dir_lib_arch, target_lib_static),
                          env.Command(target = os.path.join(dir_lib_arch, 'libwxc.so'),
                                      source = target_lib_shared,
                                      action = [sym_link]),
                          env.InstallAs(os.path.join(dir_lib_arch_pkg, 'wxc.pc'), 'wxc.pc.unix'),
                          env.Install(dir_share_doc_libwxc_dev, ['LICENSE',
                                                                 'README.md'])]
    dst_dev = env.Alias('install-dev', target_install_dev)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons deinstall                                                              #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Configure deinstallation logic
    def deinstall(nodes):
        deletes = []
        for node in nodes:
            if node.__class__ == dst[0].__class__:
                deletes.append(deinstall(node.sources))
            else:
                deletes.append(Delete(str(node)))
        return deletes

    # Add deinstall target
    target_deinstall_all = env.Command('deinstall', '', Flatten(deinstall(dst)) or '')
    env.AlwaysBuild(target_deinstall_all)
    env.Precious(target_deinstall_all)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons deinstall-dev                                                          #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Add deinstall target (development files)
    target_deinstall_dev = env.Command('deinstall-dev', '', Flatten(deinstall(dst_dev)) or '')
    env.AlwaysBuild(target_deinstall_dev)
    env.Precious(target_deinstall_dev)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons linux                                                                  #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Package names
    deb = 'libwxc_' + ver + '-1_amd64.deb'
    rpm = 'libwxc-' + ver + '-2.x86_64.rpm'
    tgz = 'libwxc-' + ver + '.tgz'

    # Add dpkg builder
    builder_dpkg = SCons.Builder.Builder(action         = 'fakeroot dpkg --build $SOURCE $TARGET',
                                         src_suffix     = '',
                                         suffix         = '.deb',
                                         source_factory = env.fs.Dir)
    env.Append(BUILDERS = {'builder_deb' : builder_dpkg})

    # Add alien builder
    builder_alien = SCons.Builder.Builder(action     = 'fakeroot alien $type $SOURCE',
                                          ENV        = os.environ, # It avoids error "Permission denied"
                                          src_suffix = '.deb')
    env.Append(BUILDERS = {'builder_rpm_or_tgz' : builder_alien})

    # Add linux target
    target_deb = env.builder_deb       (target = deb, source = 'debian/')
    target_rpm = env.builder_rpm_or_tgz(target = rpm, source = deb, type = '--to-rpm')
    target_tgz = env.builder_rpm_or_tgz(target = tgz, source = deb, type = '--to-tgz')

    env.AlwaysBuild(target_deb)

    target_linux = [ target_deb, target_rpm, target_tgz ]
    env.Alias('linux', target_linux)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons linux-dev                                                              #
################################################################################

# GNU/Linux only
if sys.platform == 'linux' or sys.platform == 'linux2':

    # Package names
    deb_dev = 'libwxc-dev_' + ver + '-1_amd64.deb'
    rpm_dev = 'libwxc-dev-' + ver + '-2.x86_64.rpm'
    tgz_dev = 'libwxc-dev-' + ver + '.tgz'

    # Add linux target (development files)
    target_deb_dev = env.builder_deb       (target = deb_dev, source = 'debian-dev/')
    target_rpm_dev = env.builder_rpm_or_tgz(target = rpm_dev, source = deb_dev, type = '--to-rpm')
    target_tgz_dev = env.builder_rpm_or_tgz(target = tgz_dev, source = deb_dev, type = '--to-tgz')

    env.AlwaysBuild(target_deb_dev)

    target_linux_dev = [target_deb_dev, target_rpm_dev, target_tgz_dev]
    env.Alias('linux-dev', target_linux_dev)

################################################################################
# TARGET:                                                                      #
#                                                                              #
# scons must_die                                                               #
################################################################################

# Windows only
if sys.platform == 'win32' or sys.platform == 'cygwin':

    # Archive name
    zip = 'libwxc_' + ver + '_amd64.zip'

    # Add zip builder
    builder_zip = SCons.Builder.Builder(action         = 'zip -r $TARGET $SOURCES',
                                        src_suffix     = '',
                                        suffix         = '.zip',
                                        source_factory = env.fs.Dir)
    env.Append(BUILDERS = {'builder_zip' : builder_zip})

    # Add must die target
    ver_win = str(major) + '-' + str(minor) + '-' + str(micro) + '-' + str(nano) + '-' + str(fork)
    target_install_win = [env.Install  ('win/wxc/',   Glob('src/include/*.h')),
                          env.Install  ('win/',       'libwxc.a'),
                          env.Install  ('win/',       'libwxc.dll.a'),
                          env.Install  ('win/',       'libwxc-' + ver_win + '.dll.a'),
                          env.Install  ('win/',       'msys-wxc-' + ver_win + '.dll'),
                          env.InstallAs('win/wxc.pc', 'wxc.pc.win')]
    target_die_must = env.builder_zip(target = zip, source = Glob('win/*'))
    env.Depends(target_die_must, target_install_win) # Should do local installation before the full installation
    env.Alias('must_die', target_die_must)

################################################################################
# scons --clean                                                                #
# scons all --clean                                                            #
################################################################################

# Configure --clean flag
env.Clean(target_all, '.sconsign.dblite')
